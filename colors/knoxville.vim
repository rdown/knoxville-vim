" Knoxville VIM Color Scheme
" Copyright (C) 2013 Robert Down
"
" This program is free software: you can redistribute it and/or modify
" it under the terms of the GNU General Public License as published by
" the Free Software Foundation, either version 3 of the License, or
" (at your option) any later version.
" 
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
" 
" You should have received a copy of the GNU General Public License
" along with this program.  If not, see <http://www.gnu.org/licenses/>.

set background=dark
highlight clear
if exists("syntax_on")
	syntax reset
endif
let g:colors_name = "Knoxville"

highlight Comment		guifg=#949494 ctermfg=249
highlight Constant		guifg=#eeeeee ctermfg=255
highlight Cursor		guibg=#d0d0d0 ctermbg=252 guifg=#eeeeee ctermfg=255 gui=NONE
highlight CursorLine 	guibg=#606060 ctermbg=241 cterm=NONE gui=NONE
highlight DiffAdd		guifg=#00af00 ctermfg=34  guibg=NONE ctermbg=NONE gui=italic, cterm=italic
highlight DiffChange	guifg=#00afff ctermfg=39  guibg=NONE ctermbg=NONE  gui=italic, cterm=italic
highlight DiffDelete	guifg=#ffaf00 ctermfg=214 guibg=NONE ctermbg=NONE gui=italic, cterm=italic  
highlight DiffText		guifg=#ffffaf ctermfg=229 guibg=NONE ctermbg=NONE gui=italic, cterm=italic
highlight Error			guifg=#ff0000 ctermfg=196 gui=bold cterm=bold guibg=none ctermbg=none
highlight ErrorMsg		guibg=#d70000 ctermfg=160 guifg=#ffffff ctermfg=15
highlight LineNr 		guifg=#b2b2b2 ctermfg=249 
highlight MatchParen	guifg=#262626 ctermfg=235 guibg=#eeeeee ctermbg=255 gui=none cterm=none
highlight MoreMsg		guifg=#5fd75f ctermfg=77  guibg=NONE ctermbg=NONE
highlight Normal 		guibg=#262626 ctermbg=235 guifg=#eeeeee ctermfg=255 gui=NONE
highlight Question		guifg=#d787ff ctermfg=177 guibg=NONE ctermbg=NONE gui=BOLD
highlight Special		guifg=#F77F00 ctermfg=202
highlight StatusLine	guibg=#282828 ctermbg=235 guifg=#eeeeee ctermfg=255 gui=bold cterm=bold
highlight StatusLineNC	guibg=#282828 ctermbg=235 guifg=#b2b2b2 ctermfg=249 gui=bold cterm=bold
highlight Title			guifg=#F77F00 ctermfg=202 gui=bold cterm=bold guibg=none ctermbg=none
highlight Todo 			guifg=#ffff00 ctermfg=226 gui=bold cterm=bold guibg=none ctermbg=none
highlight VertSplit		guifg=#4e4e4e ctermfg=239 guibg=#4e4e4e ctermbg=239 gui=bold cterm=bold

highlight default link CursorIM Cursor
highlight default link Statement Normal
