===========================================
    Knoxville VIM Color Scheme
===========================================

Knoxville is a dark VIM color scheme inspired by the wonderful colors
of the University of Tennessee. The color scheme features a charcoal background
with light grey text that is easy on the eyes. Colors are earthy, stand out
against the background.

--------------------------
    Pathogen Compliant
--------------------------

Just clone the repo into your bundle file and you're set!

---------------
    License
---------------

Knoxville VIM Color Scheme
Copyright (C) 2013 Robert Down

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
